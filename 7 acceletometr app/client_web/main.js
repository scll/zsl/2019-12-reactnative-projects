var posX = -0.1;
var posY = 0;
let lastPosZ = 0;
let arr = [];
let arr3D = [];

window.addEventListener("load", () => {
  //const WebSocket = require('ws');

  const ws = new WebSocket("ws://192.168.4.222:1337");

  //wysłanie danych na serwer przy podłączeniu klienta do serwera

  ws.onopen = () => {
    ws.send("klient się podłączył");
    console.log("klient się podłączył");
  };

  //odebranie danych z serwera i reakcja na nie, po sekundzie

  ws.onmessage = e => {
    //console.log(e.data);
    //document.getElementById("main").innerHTML = e.data;
    if (e.data == "STOP") {
      posX = 0;
      posY = 0;
    } else {
      console.log(e.data);
      let dane = JSON.parse(e.data);
      posX = dane.x;
      posY = dane.y;
    }

    // setTimeout(()=>{ws.send("get")}, 1000)
  };

  ws.onerror = e => {
    console.log(e.message);
  };

  ws.onclose = e => {
    console.log(e.code, e.reason);
  };

  // var scene = new THREE.Scene();
  // var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

  var scene = new THREE.Scene();
  var camera = new THREE.PerspectiveCamera(45, $("#root").width() / $("#root").height(), 1, 3000);
  // camera.position.set(450, 1500, 1350);
  // camera.lookAt(new THREE.Vector3(450, 0, 450));
  var renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setClearColor(0x222222, 1);
  renderer.setSize($("#root").width(), $("#root").height());

  var geometry = new THREE.CubeGeometry(50, 50, 50);
  //var geometry = new THREE.SphereGeometry(5,36,36);
  var material = new THREE.MeshPhongMaterial({
    shininess: 1,
    side: THREE.DoubleSide
  });
  var cube = new THREE.Mesh(geometry, material);
  // cube.material.color.setHex("0xff6666");
  cube.position.y = 101;
  let kolor = "0xff0000";
  // cube.material.color.setHex(kolor);
  scene.add(cube);

  camera.position.x = 0;
  camera.position.z = 800;
  camera.position.y = 1000;
  camera.lookAt(cube.position);
  // camera.rotation.x = Math.sin(Math.PI) / Math.cos(Math.PI);

  // var renderer = new THREE.WebGLRenderer();
  // renderer.setSize(window.innerWidth, window.innerHeight);
  document.getElementById("root").appendChild(renderer.domElement);

  const bottom = () => {
    let begin = -15;
    let end = 15;
    let squareGeo = new THREE.BoxGeometry(100, 300, 100);
    let squareMaterial1 = new THREE.MeshPhongMaterial({
      shininess: 1,
      side: THREE.DoubleSide
    });
    let square1 = new THREE.Mesh(squareGeo, squareMaterial1);
    for (let i = begin; i < end; i++) {
      for (let j = begin - 7; j < end + 7; j++) {
        if (
          arr.indexOf(
            (Math.round(cube.position.z / 100) * 100 + 100 * i).toString() + (Math.round(cube.position.x / 100) * 100 + 100 * j).toString()
          ) == -1
        ) {
          let square = square1.clone();
          let randomY = Math.random() * 220; // + 220
          square.position.z = Math.round(cube.position.z / 100) * 100 + 100 * i;
          square.position.x = Math.round(cube.position.x / 100) * 100 + 100 * j;
          square.position.y = -250 - (50 - randomY) / 2;
          let fade = Math.floor(Math.random() * 38) + 50;
          let kolor = "0x" + fade + fade + fade;
          square.material.color.setHex(kolor);
          scene.add(square);
          let m =
            (Math.round(cube.position.z / 100) * 100 + 100 * i).toString() + (Math.round(cube.position.x / 100) * 100 + 100 * j).toString();
          arr.push(m);
          arr3D.push(square);
        }
      }
    }
    if (arr3D.length > 2500) {
      for (let i = 0; i < 50; i++) {
        scene.remove(arr3D[0]);
        arr3D.shift();
        arr.shift();
      }
    }
  };

  let SpotLightRed = new THREE.SpotLight(0xdd4234, 2, 2000, Math.PI);
  SpotLightRed.position.y = -200;
  SpotLightRed.position.z = -200;
  SpotLightRed.position.x = -600;
  scene.add(SpotLightRed);
  let SpotLightRed2 = new THREE.SpotLight(0xdd4234, 2, 1000, Math.PI);
  SpotLightRed2.position.y = -200;
  SpotLightRed2.position.z = -600;
  SpotLightRed2.position.x = 700;
  scene.add(SpotLightRed2);
  let SpotLightBlue = new THREE.SpotLight(0x4234dd, 2, 2000, Math.PI);
  SpotLightBlue.position.y = -200;
  SpotLightBlue.position.z = 1200;
  SpotLightBlue.position.x = 1400;
  scene.add(SpotLightBlue);
  let SpotLightBlue2 = new THREE.SpotLight(0x4234dd, 2, 1000, Math.PI);
  SpotLightBlue2.position.y = -200;
  SpotLightBlue2.position.z = 1400;
  SpotLightBlue2.position.x = 100;
  scene.add(SpotLightBlue2);

  let SpotLightRedCube = new THREE.SpotLight(0xdd4234, 1, 300, Math.PI);
  SpotLightRedCube.position.y = 200;
  SpotLightRedCube.position.z = 70;
  SpotLightRedCube.position.x = 0;
  scene.add(SpotLightRedCube);

  function animate() {
    if (cube.position.y + posY * 10 > 100) {
      cube.position.y += posY * 10;

      camera.position.y += posY * 10;
      SpotLightRedCube.position.y += posY * 10;
    }
    if (Math.round(cube.position.z / 100) !== lastPosZ) {
      lastPosZ = Math.round(cube.position.z / 100);
      bottom();
    }
    SpotLightRed.position.z -= 10;
    SpotLightRed.position.x -= posX * 10;
    SpotLightRed2.position.z -= 10;
    SpotLightRed2.position.x -= posX * 10;
    SpotLightBlue.position.z -= 10;
    SpotLightBlue.position.x -= posX * 10;
    SpotLightBlue2.position.z -= 10;
    SpotLightBlue2.position.x -= posX * 10;

    SpotLightRedCube.position.z -= 10;
    SpotLightRedCube.position.x -= posX * 10;

    cube.position.x -= posX * 10;
    camera.position.x -= posX * 10;
    cube.position.z -= 10;
    camera.position.z -= 10;
    // bottom()
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
  }

  bottom();
  animate();
});
