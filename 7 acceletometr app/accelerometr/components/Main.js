import React, { Component } from "react";
import { View, Text, StyleSheet, Button } from "react-native";

class Main extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
        <View style={styles.top}>
          <Text style={styles.text}>Accelerometer</Text>
        </View>
        <View style={styles.bottom}>
          <Button title="Start" color="#f44336" onPress={() => this.props.navigation.navigate("Client")} />
        </View>
      </View>
    );
  }
}

export default Main;

const styles = StyleSheet.create({
  top: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: "#f44336"
  },
  bottom: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    backgroundColor: "#ffcdd2"
  },
  text: {
    color: "white",
    fontSize: 80
  }
});
