import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Accelerometer } from "expo-sensors";
import MyButtonRound from "./MyButtonRound";

class Client extends Component {
  static navigationOptions = {
    // header: null,
    title: "Akcelerometr",
    headerStyle: {
      backgroundColor: "#f44336"
    },
    headerTitleStyle: {
      color: "#ffcdd2"
    }
  };
  constructor(props) {
    super(props);
    this.state = {
      accelerometerState: false,
      x: 0.0,
      y: 0.0,
      z: 0.0,
      sendingStarted: false
    };
    Accelerometer.setUpdateInterval(1000);
    this.wsOpen();
  }

  wsOpen() {
    ws.onopen = () => {
      ws.send("Podłączono Aplikację AccelerometerApp");
    };
  }

  sendData(data) {
    //console.log(data)
    ws.send(JSON.stringify(data));
  }

  accelerometerPower() {
    if (!this.state.accelerometerState) {
      Accelerometer.addListener(accelerometerData => {
        this.setState({
          x: Math.round(accelerometerData.x * 10) / 10,
          y: Math.round(accelerometerData.y * 10) / 10,
          z: Math.round(accelerometerData.z * 10) / 10
        });
        let data = {
          x: Math.round(accelerometerData.x * 100) / 100,
          y: Math.round(accelerometerData.y * 100) / 100,
          z: Math.round(accelerometerData.z * 100) / 100
        };
        this.state.sendingStarted == true ? this.sendData(data) : null;
      });
    } else {
      console.log("Koniec");
      Accelerometer.removeAllListeners();
      this.setState({
        x: 0,
        y: 0,
        z: 0
      });
    }
    this.setState({
      accelerometerState: !this.state.accelerometerState
    });
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <MyButtonRound
            Press={() => {
              this.accelerometerPower();
            }}
            color="#ffcdd2"
            colorText="#f44336"
            text="Start/Stop Akcelerometr"
          />
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text style={styles.text}>x: {this.state.x}</Text>
          <Text style={styles.text}>y: {this.state.y}</Text>
          <Text style={styles.text}>z: {this.state.z}</Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <MyButtonRound
            Press={() => {
              console.log("OPEN");
              this.state.sendingStarted == true ? ws.send("STOP") : null;
              this.setState({
                sendingStarted: !this.state.sendingStarted
              });
            }}
            color="#f44336"
            colorText="#ffcdd2"
            text="Start/Stop Wysyłanie"
          />
        </View>
      </View>
    );
  }
}

export default Client;

const styles = StyleSheet.create({
  text: {
    fontSize: 40,
    color: "black"
  }
});

const ws = new WebSocket("ws://192.168.43.124:1337");
