import PropTypes from "prop-types";
import { Image, StyleSheet, TouchableOpacity, View, Text } from "react-native";
import React, { Component } from "react";

class MyButtonRound extends Component {
  constructor(props) {
    super(props);
    //console.log(this.props)
    const vibrate_click = 100;
  }
  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.Press();
        }}
      >
        <View style={styles(this.props.color, this.props.colorText).button}>
          <Text style={styles().text}>{this.props.text}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

MyButtonRound.propTypes = {
  Press: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired
};

const styles = (color, colorText) =>
  StyleSheet.create({
    button: {
      width: 300,
      height: 300,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: color,
      borderRadius: 1000,
      padding: 0,
      margin: 0
    },
    text: {
      color: colorText,
      fontSize: 40,
      textAlignVertical: "center",
      textAlign: "center"
    }
  });
export default MyButtonRound;
