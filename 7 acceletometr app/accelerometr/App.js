import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";

import Main from "./components/Main";
import Client from "./components/Client";

const Root = createStackNavigator({
  Main: { screen: Main },
  Client: { screen: Client }
});

const App = createAppContainer(Root);

export default App;
