import * as SQLite from "expo-sqlite"

const db = SQLite.openDatabase("Szczepaniak_Przemyslaw_4ic1.db") // proszę o taki schemat nazywania swojej bazy danych

export default class Database {
  static createTable() {
    db.transaction(tx => {
      tx.executeSql("CREATE TABLE IF NOT EXISTS alarms (id integer primary key not null, godzina text, dni text);")
      // console.log("CO")
    })
  }
  static add(godzina, dni) {
    db.transaction(
      tx => {
        tx.executeSql(`INSERT INTO alarms (godzina, dni) values ('${godzina}', '${dni}')`)
      },
      err => console.log("AAA", JSON.stringify(err))
    ),
      () => console.log("dodano rekord do bazy!")
  }
  static getAll() {
    let query = "SELECT * FROM alarms"
    return new Promise((resolve, reject) =>
      db.transaction(tx => {
        tx.executeSql(
          query,
          [],
          (tx, results) => {
            // console.log(JSON.stringify(results))
            resolve(JSON.stringify(results))
          },
          function(tx, error) {
            reject(error)
          }
        )
      })
    )
  }
  static remove(id) {
    db.transaction(tx => {
      tx.executeSql(`DELETE FROM alarms WHERE (id = ${id});`)
    })
  }
  static removeAll() {
    db.transaction(tx => {
      tx.executeSql("DELETE FROM alarms ;")
    })
  }
}
