import React from "react"
import { Text, TouchableOpacity } from "react-native"

const Button = ({ text, action, textStyle, btStyle }) => {
  return (
    <TouchableOpacity
      onPress={action}
      style={{
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        ...btStyle
      }}
    >
      <Text style={{ ...textStyle }}>{text}</Text>
    </TouchableOpacity>
  )
}

export default Button
