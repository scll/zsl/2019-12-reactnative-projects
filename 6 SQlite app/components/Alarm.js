import React from "react";
import { StyleSheet, Text, View, Switch, Vibration } from "react-native";
import Button from "./Button";
import { withNavigation } from "react-navigation";
import { Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Animated } from "react-native";
import { Audio } from "expo-av";
const soundObject = new Audio.Sound();

const Screen = ({ station, navigation, delFunc, delID, hour, checkFunc, indx, time }) => {
  // console.log("indx", indx)
  const [visible, isVisible] = React.useState(true);
  const del = () => {
    isVisible(false);
    delFunc(delID);
  };
  const [height, swapHeight] = React.useState(new Animated.Value(30)); // początkowa wartość wysokości itema
  const [expanded, isExpanded] = React.useState(false); // zwinięty
  const [val, setVal] = React.useState(false);
  const toggle = () => {
    let b;
    if (!expanded) b = 70;
    else b = 30;
    Animated.spring(height, {
      toValue: b
    }).start();
    const m = !expanded;
    isExpanded(m);
  };
  React.useEffect(() => {
    const d_hours = hour.split(":")[0];
    const d_mins = hour.split(":")[1];
    if (time.hours == d_hours && time.mins == d_mins) {
      if (val) {
        let LoadMP3 = async () => {
          await soundObject.unloadAsync();
          await soundObject.loadAsync(require("./ft.mp3"));
          await soundObject.playAsync();
        };
        LoadMP3();
        Vibration.vibrate(100);
      } else {
        Vibration.cancel();
        soundObject.stopAsync();
      }
    }
  }, [time]);
  const [days, updateDays] = React.useState([false, false, false, false, false, false, false]);
  const daysShort = ["PN", "WT", "ŚR", "CZ", "PT", "SB", "ND"];
  const daysLong = ["pon.,", "wto.,", "śro.,", "czw.,", "pią.,", "sob.,", "nie."];
  return visible ? (
    <View style={styles.main}>
      <View
        style={{
          flex: 1,
          justifyContent: "space-between",
          alignItems: "center",
          flexDirection: "row",
          height: 100
        }}
      >
        <Text style={{ flex: 1, fontWeight: "bold", fontSize: 60 }}>{hour}</Text>
        <Switch
          value={val}
          onValueChange={async e => {
            await setVal(e);
            checkFunc(e, indx);
          }}
        />
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: "space-between",
          alignItems: "center",
          flexDirection: "row",
          height: 50,
          width: "100%"
        }}
      >
        <View style={{ width: 50, height: 50 }}>
          <TouchableOpacity onPress={() => del()}>
            <Feather name={"trash-2"} size={25} color="#f44336" />
          </TouchableOpacity>
        </View>
        <View style={{ width: 50, height: 50 }}>
          <TouchableOpacity onPress={() => toggle()}>
            <Feather name={"chevron-down"} size={25} color="#f44336" />
          </TouchableOpacity>
        </View>
      </View>
      <Animated.View
        style={{
          height: height, // animowany styl, tutaj wysokość View
          flex: 1,
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "row",
          width: "100%"
        }}
      >
        {expanded
          ? days.map((e, index) => (
              <View
                key={index}
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  width: 50,
                  height: 50,
                  backgroundColor: days[index] ? "#FF000049" : "#FF000009",
                  borderRadius: 30
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    const copy = [...days];
                    copy[index] = !copy[index];
                    updateDays(copy);
                  }}
                >
                  <Text>{daysShort[index]}</Text>
                </TouchableOpacity>
              </View>
            ))
          : days.map(
              (e, index) =>
                days[index] && (
                  <View
                    key={index}
                    style={{
                      justifyContent: "center",
                      alignItems: "center",
                      width: 50,
                      height: 30,
                      backgroundColor: "#FF000019"
                    }}
                  >
                    <Text>{daysLong[index]}</Text>
                  </View>
                )
            )}
      </Animated.View>
    </View>
  ) : null;
};

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#ffc9d2",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%"
  }
});

export default withNavigation(Screen);
