import React from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import CircleButton from "./CircleButton";
import CircleButtonText from "./CircleButtonText";
import Button from "./Button";
import Database from "../Database";

export default function Screen({ navigation }) {
  const [min, setMin] = React.useState(0);
  const [hour, setHour] = React.useState(0);
  const [isMinute, isMinuteSet] = React.useState(true);
  let minutes = [];
  let hours = [];
  for (let i = 0; i < 60; i++) {
    minutes.push(i);
  }
  for (let i = 0; i < 24; i++) {
    hours.push(i);
  }
  const minCircle = minutes.map((e, index) => (
    <CircleButtonText
      large={false}
      func={() => setMin(e)}
      btStyle={{
        flex: 1,
        position: "absolute",
        top:
          Dimensions.get("window").height / 4 +
          (Math.sin(((2 * Math.PI) / 60) * (index - 15)) * Dimensions.get("window").width) / 2.5 +
          250,
        left:
          Dimensions.get("window").width / 2 + (Math.cos(((2 * Math.PI) / 60) * (index - 15)) * Dimensions.get("window").width) / 2.5 - 60
      }}
      img={"plus"}
      key={index}
      text={e}
    />
  ));
  const hourCircle = hours.map((e, index) => (
    <CircleButtonText
      large={false}
      func={() => setHour(e)}
      btStyle={{
        flex: 1,
        position: "absolute",
        top:
          Dimensions.get("window").height / 4 + (Math.sin(((2 * Math.PI) / 24) * (index - 6)) * Dimensions.get("window").width) / 2.5 + 250,
        left:
          Dimensions.get("window").width / 2 + (Math.cos(((2 * Math.PI) / 24) * (index - 6)) * Dimensions.get("window").width) / 2.5 - 60,
        backgroundColor: "white",
        borderRadius: 100
      }}
      img={"plus"}
      key={index}
      text={e}
    />
  ));
  // console.log(isMinute)
  return (
    <View style={styles.main}>
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignContent: "center",
          alignItems: "center",
          position: "absolute",
          top: 0
        }}
      >
        <Button
          textStyle={{ fontSize: 170 }}
          text={hour.toString()[0] && !hour.toString()[1] ? "0" + hour.toString() : hour.toString()}
          action={() => isMinuteSet(false)}
        />
        <Text style={{ fontSize: 170, margin: 20 }}>:</Text>
        <Button
          textStyle={{ fontSize: 170 }}
          text={min.toString()[0] && !min.toString()[1] ? "0" + min.toString() : min.toString()}
          action={() => isMinuteSet(true)}
        />
      </View>
      {isMinute ? minCircle : hourCircle}
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignContent: "center",
          alignItems: "center",
          position: "absolute",
          bottom: 0
        }}
      >
        <CircleButton
          large={true}
          func={async () => {
            await Database.add(
              `${hour.toString()[0] && !hour.toString()[1] ? "0" + hour.toString() : hour.toString()}:${
                min.toString()[0] && !min.toString()[1] ? "0" + min.toString() : min.toString()
              }`,
              "[]"
            );
            navigation.navigate("s2");
          }}
          btStyle={{ flex: 1 }}
          img={"plus"}
        />
      </View>
    </View>
  );
}

Screen.navigationOptions = {
  title: "Ustaw budzik",
  headerStyle: {
    backgroundColor: "#f44336"
  },
  headerTitleStyle: {
    color: "#ffcdd2"
  }
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#ffcdd2",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  main2: {
    flex: 1,
    alignContent: "center",
    justifyContent: "space-around",
    alignItems: "center"
  },
  container1: {
    height: 50,
    width: Dimensions.get("window").width,
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row"
  }
});
