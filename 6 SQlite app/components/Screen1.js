import React, { useEffect } from "react"
import { StyleSheet, Text, View, ActivityIndicator } from "react-native"
import * as Font from "expo-font"
import Button1 from "./Button"
import Database from "../Database"

export default function Screen({ navigation }) {
  const [fontloaded, fontLoadedUpdate] = React.useState(false)
  const load = async () => {
    await Font.loadAsync({
      myfont: require("./Vegan.ttf")
    })
    fontLoadedUpdate(true)
  }
  const load2 = async () => {
    await Database.createTable()
    // console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
  }
  // console.log("m");
  useEffect(() => {
    load()
    load2()
  }, [])
  return fontloaded ? (
    <View style={styles.main}>
      <View style={styles.container1}>
        <Text style={styles.c3}></Text>
        <Text style={styles.c1}>Sqlite App</Text>
        <Text style={styles.c2}>Manage Sqlite</Text>
        <Text style={styles.c2}>Use animation</Text>
        <Text style={styles.c2}>Use ring</Text>
        <Text style={styles.c3}></Text>
      </View>
      <View style={styles.container2}>
        <Button1
          text={"Start"}
          textStyle={{
            fontSize: 50,
            fontFamily: "myfont",
            color: "#ffcdd2",
            width: "25%"
          }}
          action={() => {
            navigation.navigate("s2")
          }}
        />
      </View>
    </View>
  ) : (
    <View style={styles.main2}>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
  )
}

Screen.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
  main: {
    flex: 1
  },
  main2: {
    flex: 1,
    backgroundColor: "#f44336",
    alignContent: "center",
    justifyContent: "space-around",
    alignItems: "center"
  },
  container1: {
    flex: 2,
    backgroundColor: "#f44336"
  },
  container2: {
    flex: 1,
    backgroundColor: "#f44336"
  },
  button1: {
    fontSize: 50,
    fontFamily: "myfont",
    color: "#ffcdd2"
  },
  c1: {
    flex: 5,
    fontSize: 70,
    textAlign: "center",
    textAlignVertical: "center",
    fontFamily: "myfont",
    color: "#ffcdd2"
  },
  c2: {
    flex: 2,
    fontSize: 30,
    textAlign: "center",
    textAlignVertical: "center",
    fontFamily: "myfont",
    color: "#ffcdd2"
  },
  c3: {
    flex: 5
  },
  inputHeader: {
    height: 60,
    color: "#f44336"
  }
})
