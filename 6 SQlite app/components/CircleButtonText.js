import React from "react"
import { TouchableOpacity, Text } from "react-native"
import { Feather } from "@expo/vector-icons"

const CircleButton = ({ func, btStyle, img, large, text }) => {
  let h = large ? 98 : 80
  return (
    <TouchableOpacity
      onPress={() => func()}
      style={{
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        height: h,
        width: h,
        margin: 20,
        ...btStyle
      }}
    >
      <Text
        style={{ fontSize: h / 4, backgroundColor: "white", borderRadius: 100 }}
      >
        {text}
      </Text>
    </TouchableOpacity>
  )
}

export default CircleButton
