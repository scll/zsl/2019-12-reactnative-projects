import React from "react";
import { StyleSheet, Text, View, FlatList, Dimensions, Vibration } from "react-native";
import CircleButton from "./CircleButton";
import Database from "../Database";
import Alarm from "./Alarm";

export default function Screen({ navigation }) {
  const [alarmsList, alarmsUpdate] = React.useState([]);
  const [alarmsState, setAlarmsState] = React.useState([]);
  const [time, setTime] = React.useState({});
  const updateSelected = (state, indx) => {
    // console.log("UPD", alarmsState)
    // console.log(state, indx)
    let m = [...alarmsState];
    m[indx] = state;
    setAlarmsState(m);
  };
  // console.log(alarmsState)

  const intervall = () => {
    setInterval(() => {
      let mins = new Date().getMinutes().toString();
      let hours = new Date().getHours().toString();
      setTime({ hours, mins });
    }, 1000);
  };
  React.useEffect(() => {
    intervall();
  }, []);
  const m = () => {
    alarmsUpdate([]);
    navigation.navigate("s3");
  };
  const getAllF = () => {
    Database.getAll().then(all => {
      alarmsUpdate(JSON.parse(all).rows._array);
      let m = [];
      for (let i = 0; i < JSON.parse(all).rows._array.length; i++) m.push(false);
      setAlarmsState(m);
    });
  };
  React.useEffect(() => {
    Database.createTable();
    getAllF();
  }, []);

  navigation.addListener("didFocus", () => {
    getAllF();
  });

  const del = id => {
    Database.remove(id);
    // getAllF()
  };

  return (
    <View style={styles.main}>
      <FlatList
        data={alarmsList}
        renderItem={({ item, index }) => (
          <Alarm delID={item.id} delFunc={del} hour={item.godzina} checkFunc={updateSelected} indx={index} time={time} />
        )}
        keyExtractor={item => JSON.stringify(item.id)}
        style={{
          width: Dimensions.get("screen").width
        }}
      />
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignContent: "center",
          alignItems: "center",
          position: "absolute",
          bottom: 0
        }}
      >
        <CircleButton large={false} func={() => m()} btStyle={{ flex: 1 }} img={"plus"} />
      </View>
    </View>
  );
}

Screen.navigationOptions = {
  title: "Lista budzików",
  headerStyle: {
    backgroundColor: "#f44336"
  },
  headerTitleStyle: {
    color: "#ffcdd2"
  }
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#ffcdd2",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    padding: 10
  },
  container1: {
    flex: 1,
    fontSize: 50,
    textAlign: "center",
    textAlignVertical: "center",
    backgroundColor: "#ffcdd2",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  container2: {
    flex: 1,
    alignContent: "space-around",
    justifyContent: "space-around",
    alignItems: "center"
  },
  inputHeader: {
    height: 60,
    width: 200,
    color: "#f44336"
  },
  input: {
    height: 60,
    fontSize: 20,
    borderStyle: "solid",
    borderBottomColor: "#f44336",
    borderBottomWidth: 2
  }
});
