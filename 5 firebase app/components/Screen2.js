import React from "react";
import { StyleSheet, Text, View, TextInput, Image } from "react-native";
import Button from "./Button";
import firebase from "firebase";

export default function Screen({ navigation }) {
  const [nick, onChangeNick] = React.useState("test1@p.pl");
  const [pass, onChangePass] = React.useState("testtest");
  const [isLoginScreen, changeScreenState] = React.useState(true);
  const [bt1Text, updateBt1] = React.useState("Zaloguj się");
  const [bt2Text, updateBt2] = React.useState("Nie masz konta? Zarejestruj się");
  const changeScreen = () => {
    const x = !isLoginScreen;
    changeScreenState(x);
    x ? (updateBt1("Zaloguj się"), updateBt2("Nie masz konta? Zarejestruj sie")) : (updateBt1("Zarejestruj się"), updateBt2("Masz już konto? Zaloguj się"));
  };
  const enter = (name, pw) => {
    isLoginScreen
      ? firebase
          .auth()
          .signInWithEmailAndPassword(name, pw)
          .then(() => navigation.navigate("s3"))
          .catch(error => alert(error.message))
      : firebase
          .auth()
          .createUserWithEmailAndPassword(name, pw)
          .then(() => changeScreen())
          .catch(error => alert(error.message));
  };
  return (
    <View style={styles.main}>
      <View style={styles.container1}>
        <Image style={{ width: 200, height: 200, borderRadius: 100, borderWidth: 2, borderColor: "black" }} source={require("./pico.png")} />
      </View>
      <View style={styles.container2}>
        <View style={{ flex: 1 }}>
          <TextInput style={styles.input} onChangeText={e => onChangeNick(e)} value={nick} type="text" />
          <Text style={styles.inputHeader}>username</Text>
          <TextInput style={styles.input} onChangeText={e => onChangePass(e)} secureTextEntry={true} value={pass} type="password" />
          <Text style={styles.inputHeader}>password</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Button btStyle={{ marginTop: 20 }} text={bt1Text} action={() => enter(nick, pass)} />
          <Button btStyle={{ marginTop: 20 }} text={bt2Text} action={() => changeScreen()} />
        </View>
      </View>
    </View>
  );
}

Screen.navigationOptions = {
  header: null
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#ffcdd2",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    padding: 10
  },
  container1: {
    flex: 1,
    fontSize: 50,
    textAlign: "center",
    textAlignVertical: "center",
    backgroundColor: "#ffcdd2",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  container2: {
    flex: 1,
    alignContent: "space-around",
    justifyContent: "space-around",
    alignItems: "center"
  },
  inputHeader: {
    height: 60,
    width: 200,
    color: "#f44336"
  },
  input: {
    height: 60,
    fontSize: 20,
    borderStyle: "solid",
    borderBottomColor: "#f44336",
    borderBottomWidth: 2
  }
});
