import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Button from "./Button";
import { withNavigation } from "react-navigation";

const Screen = ({ station, navigation }) => (
  <View style={styles.main}>
    <View style={{ flex: 1, justifyContent: "flex-start", alignItems: "center", flexDirection: "row" }}>
      <Text style={{ flex: 1, fontWeight: "bold" }}> {station.stationName} </Text>
    </View>
    <View style={{ flex: 1, justifyContent: "center", alignContent: "space-around", flexDirection: "row" }}>
      <View style={{ flex: 1 }}>
        <Text> lat: {station.latitude} </Text>
        <Text> lng: {station.longitude} </Text>
        <Text> Razem stacji: {station.totalDocks} </Text>
      </View>
      <View style={{ flex: 1, justifyContent: "center" }}>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: station.availableBikes, backgroundColor: "black" }}>
            <Text style={{ color: "white", textAlign: "center" }}>R</Text>
          </View>
          <View style={{ flex: station.availableDocks, backgroundColor: "grey" }}>
            <Text style={{ color: "white", textAlign: "center" }}>S</Text>
          </View>
        </View>
        <View style={{ flex: 1, backgroundColor: station.statusValue === "In Service" ? "green" : "yellow" }}>
          <Text style={{ color: "white", textAlign: "center" }}>{station.statusValue === "In Service" ? "DOSTEPNA" : "W NAPRAWIE"}</Text>
        </View>
      </View>
    </View>
    <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "center", flexDirection: "row", width: "100%" }}>
      <Button
        btStyle={{ width: 100 }}
        text={"MAPA"}
        action={() => navigation.navigate("mapa", { name: station.stationName, lat: station.latitude, lng: station.longitude })}
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#ffc9d2",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    height: 150,
    width: "100%"
  }
});

export default withNavigation(Screen);
