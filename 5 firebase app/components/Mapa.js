import React from "react";
import { View } from "react-native";
import MapView from "react-native-maps";

export default function Screen({ navigation }) {
  const { name, lng, lat } = navigation.state.params;
  console.log(navigation.state.params);
  return (
    <View style={{ flex: 1 }}>
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: 40.7221,
          longitude: -73.9972,
          latitudeDelta: 0.1,
          longitudeDelta: 0.1
        }}
      >
        <MapView.Marker
          coordinate={{
            latitude: lat,
            longitude: lng
          }}
          title={name}
        />
      </MapView>
    </View>
  );
}

Screen.navigationOptions = {
  // header: null,
  title: "Lokalizacja na mapie",
  headerStyle: {
    backgroundColor: "#f44336"
  },
  headerTitleStyle: {
    color: "#ffcdd2"
  }
};
