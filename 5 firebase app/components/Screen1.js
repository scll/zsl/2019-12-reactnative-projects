import React, {useEffect} from "react"
import { StyleSheet, Text, View, ActivityIndicator } from "react-native"
import * as Font from "expo-font"
import Button1 from "./Button"
import firebase from "firebase"

let config = {
  apiKey: "AIzaSyB1FbrJ-1sZ4EGpBbKZkHRzIOodHYlF4lA",
  authDomain: "szczepaniak4ic1.firebaseapp.com",
  databaseURL: "https://szczepaniak4ic1.firebaseio.com",
  projectId: "szczepaniak4ic1",
  storageBucket: "szczepaniak4ic1.appspot.com",
  messagingSenderId: "562033929979",
  appId: "1:562033929979:web:b5f165722b0071c3790a5e"
};
firebase.initializeApp(config);

export default function Screen({navigation}) {
  const [fontloaded, fontLoadedUpdate] = React.useState(false)
  const [isLogged, changeLoginState] = React.useState(false)
  const load = async () => {
    await Font.loadAsync({
        'myfont': require('./Vegan.ttf'),
    });
    firebase.auth().onAuthStateChanged(user => {
      alert(JSON.stringify("logged user email: "+user))
      // jeśli user istnieje, wtedy przechodzimy do wyświetlenia ekranu z listą danych pobranych z bazy firebase
      // jeśli nie istnieje - wtedy przechodzimy do ekranu logowania
      user ? changeLoginState(true) : changeLoginState(false)
      fontLoadedUpdate(true)
   })
  }
  useEffect(() => {
    load();
  }, []);
  return fontloaded ? (
      <View style={styles.main}>
          <View style={styles.container1}>
              <Text style={styles.c3}></Text>
              <Text style={styles.c1}>Firebase App</Text>
              <Text style={styles.c2}>Firebase database and authentication</Text>
              <Text style={styles.c3}></Text>
          </View>
          <View style={styles.container2}>
            <Button1 text={"Start"} 
              textStyle={{
                fontSize: 50,
                fontFamily: 'myfont',
                color: '#ffcdd2',
                width: "35%"
              }} action={()=>{
                isLogged ? navigation.navigate("s3") : navigation.navigate("s2")
              }} />
          </View>
      </View>
  ) : (
    <View style={styles.main2}>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
  )
}

Screen.navigationOptions = {
    header: null,
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  main2: {
    flex: 1,
    backgroundColor: "#f44336",
    alignContent: "center",
    justifyContent: "space-around",
    alignItems: "center"
  },
  container1: {
    flex: 2,
    backgroundColor: '#f44336',
  },
  container2: {
    flex: 1,
    backgroundColor: '#f44336',
  },
  button1: {
    fontSize: 50,
    fontFamily: 'myfont',
    color: '#ffcdd2',
  },
  c1: {
    flex: 2,
    fontSize: 70,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontFamily: "myfont",
    color: '#ffcdd2',
  },
  c2: {
    flex: 2,
    fontSize: 30,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontFamily: "myfont",
    color: '#ffcdd2',
  },
  c3: {
    flex: 3,
  },
  inputHeader: {
    height: 60,
    color: '#f44336',
  },
})
