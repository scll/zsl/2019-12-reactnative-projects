import React from "react";
import { StyleSheet, Text, View, Dimensions, FlatList, ActivityIndicator } from "react-native";
import Button from "./Button";
import firebase from "firebase";
import Station from "./Station";

export default function Screen({ navigation }) {
  const [dataL, dataLoaded] = React.useState(false);
  const [dataBikes, dataLoad] = React.useState([]);
  const downloadData = async () => {
    let cokolwiek = await firebase.database().ref("stations_big");
    const tabs = [];
    await cokolwiek.on("value", e => {
      e.forEach(item => {
        tabs.push(JSON.parse(JSON.stringify(item)));
      });
      dataLoad(tabs);
    });
    dataLoaded(true);
  };
  React.useEffect(() => {
    downloadData();
  }, []);

  return (
    <View style={styles.main}>
      <View style={styles.container1}>
        <Text style={{ textAlign: "center", textAlignVertical: "center", flex: 2 }}>
          Zalogowano jako: {firebase.auth().currentUser.email}
        </Text>
        <Button
          btStyle={{ flex: 1 }}
          text={"WYLOGUJ"}
          action={() => {
            firebase
              .auth()
              .signOut()
              .then(() => navigation.navigate("s1"))
              .catch(error => alert(error));
          }}
        />
      </View>
      {dataL ? (
        <FlatList
          contentContainerStyle={{ width: Dimensions.get("window").width, paddingRight: 10, paddingLeft: 10 }}
          data={dataBikes}
          renderItem={({ item }) => <Station station={item} />}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <View style={styles.main2}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )}
    </View>
  );
}

Screen.navigationOptions = {
  title: "Stacje rowerowe",
  headerStyle: {
    backgroundColor: "#f44336"
  },
  headerTitleStyle: {
    color: "#ffcdd2"
  }
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#ffcdd2",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  main2: {
    flex: 1,
    alignContent: "center",
    justifyContent: "space-around",
    alignItems: "center"
  },
  container1: {
    height: 50,
    width: Dimensions.get("window").width,
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row"
  }
});
