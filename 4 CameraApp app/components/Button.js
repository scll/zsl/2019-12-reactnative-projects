import React from "react";
import { View, Text, TouchableOpacity } from "react-native";

const Button = ({ text, func, user, login, textStyle, btStyle }) => {
  return (
    <TouchableOpacity
      onPress={() => func(user, login)}
      style={{
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        height: 30,
        margin: 20,
        ...btStyle
      }}
    >
      <Text style={{ ...textStyle }}>{text}</Text>
    </TouchableOpacity>
  );
};

export default Button;
