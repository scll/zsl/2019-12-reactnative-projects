import React from "react";
import { TouchableOpacity, Text, Image, View } from "react-native";
import { Dimensions } from "react-native";
import { Feather } from "@expo/vector-icons"; 


const Record = ({ data, index, width, func, plus, funcLong }) => {
  return (
    <TouchableOpacity
      key={index}
      style={{
        marginTop: 10,
        marginBottom: 10
      }}
      onPress={func}
      onLongPress={funcLong}
    >
      <Image
        style={{
          width: width,
          height: Dimensions.get("window").height / 7
        }}
        source={{ uri: data.uri }}
      />
      <View style={{ flex: 1, 
        position: "absolute",
      alignContent: "center",
      justifyContent: "center",
      alignItems: "center" }}>
        <Feather name={"plus"} size={Dimensions.get("window").height / 7} color="#f44336" style={{
          flex: 1, opacity: plus ? 1 : 0, 
        alignContent: "center",
        justifyContent: "center",
        alignItems: "center" 
        }} />
      </View>
      <Text style={{ color: "#dddddd", position: "absolute", bottom: 5 }}>
        {data.id}
      </Text>
    </TouchableOpacity>
  );
};

export default Record;
