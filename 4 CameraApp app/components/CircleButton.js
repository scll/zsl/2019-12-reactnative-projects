import React from "react";
import { TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons"; 

const CircleButton = ({
  func,
  btStyle,
  img,
  large
}) => {
  let h = large ? 88 : 70  
  return (
    <TouchableOpacity
      onPress={() => func()}
      style={{
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        height: h,
        width: h,
        margin: 20,
        ...btStyle
      }}
    >
      <Feather name={img} size={h} color="#f44336" />
    </TouchableOpacity>
  );
};

export default CircleButton;
