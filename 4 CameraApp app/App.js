
import {createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from "./screens/HomeScreen";
import GeoDataList from "./screens/PhotosList";
import CameraScreen from "./screens/CameraScreen";
import OnePhoto from "./screens/OnePhoto";

const Root = createStackNavigator({
  s1: { screen: HomeScreen },
  s2: { screen: GeoDataList },
  s3: { screen: OnePhoto },
  camera: { screen: CameraScreen },
});
 
const App = createAppContainer(Root);

export default App;
