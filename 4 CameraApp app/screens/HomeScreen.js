import React, { useState, useEffect } from "react";
import * as Font from "expo-font";
import {
  View,
  Text,
  ActivityIndicator,
  Platform
} from "react-native";
import Button from "../components/Button";

import * as Permissions from "expo-permissions";

const HomeScreen = ({ navigation }) => {
  const [loading, cLoading] = useState(true);
  useEffect(() => {
    let xd = async () => {
      await Font.loadAsync({
        myfont: require("../Vegan.ttf")
      });
      cLoading(false);
    };
    xd();
  }, []);
  let permissionsSet = () => {
    const setPermissions = async () => {
      let { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted" && Platform.OS != "web") {
        alert(Platform.OS);
      } else {
        navigation.navigate("s2");
      }
    };
    setPermissions();
  };
  return (
    <View style={{ flex: 1, backgroundColor: "#f44336" }}>
      {loading ? (
        <ActivityIndicator size="large" color="#ffcdd2" />
      ) : (
        <View
          style={{
            backgroundColor: "#f44336",
            flex: 4,
            alignContent: "center",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text
            style={{ fontFamily: "myfont", color: "#ffcdd2", fontSize: 40 }}
          >
            Camera App
          </Text>
          <Text style={{ fontFamily: "myfont", color: "#ffcdd2", fontSize: 30 }}>show gallery pictures</Text>
          <Text style={{ fontFamily: "myfont", color: "#ffcdd2", fontSize: 30 }}>take picture from camera</Text>
          <Text style={{ fontFamily: "myfont", color: "#ffcdd2", fontSize: 30 }}>save photo to device</Text>
          <Text style={{ fontFamily: "myfont", color: "#ffcdd2", fontSize: 30 }}>delete photo from device</Text>
        </View>
      )}

      <View
        style={{
          flex: 1,
          alignContent: "flex-start",
          justifyContent: "flex-start",
          alignItems: "center",
        }}
      >
        <Button
          style={{ fontFamily: "myfont"}}
          textStyle={{ color: "#ffcdd2", fontSize: 61 }}
          text={"START"}
          func={permissionsSet}
        />
      </View>
    </View>
  );
};
HomeScreen.navigationOptions = {
  header: null
};
export default HomeScreen;
