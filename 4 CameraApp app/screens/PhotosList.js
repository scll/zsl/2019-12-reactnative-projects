import React, { useState } from "react";
import {
  View,
  FlatList,
} from "react-native";
import Button from "../components/Button";
import Record from "../components/FotoItem";
import { Dimensions } from "react-native";

import * as MediaLibrary from "expo-media-library";
let gl = false;

const PhotosList = ({ navigation }) => {
  let [pictures, cPictures] = useState([]);
  let [colnum, ccolnum] = useState(4);
  let [cwidth, ccwidth] = useState(Dimensions.get("window").width / 4);
  let [selected, updateSelected] = useState([])
  let [oneShowing, oneUpdate] = useState(false)
  let [oneSelected, oneSelUpdate] = useState([])

  navigation.addListener("didFocus", () => {
    const getData = async () => {
      let obj = await MediaLibrary.getAssetsAsync({
        first: 240, // ilość pobranych assetów
        mediaType: "photo" // typ pobieranych danych, photo jest domyślne
      });
      cPictures(obj.assets);
    };
    getData();
  });

  const cDisplay = () => {
    gl = !gl;
    if (gl) {
      ccolnum(1);
      ccwidth(Dimensions.get("window").width);
    } else {
      ccolnum(4);
      ccwidth(Dimensions.get("window").width / 4);
    }
  };
  const select = id => {
    const newSelected = [...selected]
    const lastIndex = newSelected.lastIndexOf(id)
    lastIndex === -1 ? newSelected.push(id) : newSelected.splice(lastIndex, 1)
    updateSelected(newSelected) 
  };
  const showOne = photo => {
    // oneSelUpdate([photo.id, photo.uri])
    navigation.navigate("s3", {id: photo.id, uri: photo.uri, h: photo.height, w: photo.width})
    console.log("LONG")
    // oneUpdate(true) 
  }
  // const delOne = async id => {
  //   await MediaLibrary.deleteAssetsAsync([id]);
  //   const getData = async () => {
  //     let obj = await MediaLibrary.getAssetsAsync({
  //       first: 240, // ilość pobranych assetów
  //       mediaType: "photo" // typ pobieranych danych, photo jest domyślne
  //     });
  //     cPictures(obj.assets);
  //     oneUpdate(false)
  //   };
  //   getData();

  // }
  const goCam = () => {
    navigation.navigate("camera");
  };
  // if(!oneShowing) {
    return (
      <>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <Button
            text={"Grid/list"}
            func={cDisplay}
            btStyle={{ flex: 1 }}
            textStyle={{ fontWeight: "bold" }}
          />
          <Button
            text={"Go to camera"}
            func={goCam}
            btStyle={{ flex: 1 }}
            textStyle={{ fontWeight: "bold" }}
          />
          <Button
            text={"Delete selected"}
            func={async ()=>{
              await MediaLibrary.deleteAssetsAsync(selected);
              const getData = async () => {
                let obj = await MediaLibrary.getAssetsAsync({
                  first: 240, // ilość pobranych assetów
                  mediaType: "photo" // typ pobieranych danych, photo jest domyślne
                });
                cPictures(obj.assets);
              };
              getData();
            }}
            btStyle={{ flex: 1 }}
            textStyle={{ fontWeight: "bold" }}
          />
        </View>
        <View
          style={{
            flex: 9,
            overflowY: "scroll",
            height: Dimensions.get("window").height - 50
          }}
        >
          <FlatList
            contentContainerStyle={{
              paddingBottom: 20
            }}
            numColumns={colnum}
            key={colnum}
            style={{ flex: 1 }}
            keyExtractor={(item, index) => index.toString()}
            data={pictures}
            renderItem={({ item, index }) => (
              <View style={{
                opacity: selected.indexOf(item.id) === -1 ? 1 : 0.3
              }}>
                <Record
                  index={index}
                  data={item}
                  width={cwidth}
                  func={()=>{select(item.id)}}
                  funcLong={()=>{showOne(item)}}
                  plus={selected.indexOf(item.id) === -1 ? false : true}
                />
              </View>
            )}
          />
        </View>
      </>
    ) 
    // else return (
      // <View style={{ flex: 1, flexDirection: "row" }}>
      //   <Photo uri={oneSelected[1]} width={cwidth} del={()=>delOne(oneSelected[0])}/>
      // </View>
    // );
};
PhotosList.navigationOptions = {
  headerStyle: {
    backgroundColor: "#f44336"
  },
  headerTitleStyle: {
    color: "#ffcdd2",
    textAlign: "center"
  },
  title: "Zdjęcia zapisane w telefonie"
};
export default PhotosList;
