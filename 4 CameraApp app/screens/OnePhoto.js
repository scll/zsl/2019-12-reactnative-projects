import React, { useState } from "react";
import {
  View, Image
} from "react-native";
import Button from "../components/Button";
import * as MediaLibrary from "expo-media-library";
import { Dimensions, Text } from "react-native";


const Photo = ({ navigation, width }) => {
    console.log(navigation.state.params)
    const delOne = async id =>{   
        await MediaLibrary.deleteAssetsAsync([id]);
        navigation.navigate("s2")
    }
    return (
        <View style={{ flex: 1, flexDirection: "column",
        alignContent: "space-around",
        justifyContent: "center",
        alignItems: "center" }}>
          <View style={{position: "absolute",top:0,left:100, zIndex: 5}}>
            <Text style={{fontSize: 30, color: "white"}}>{navigation.state.params.w}x{navigation.state.params.h}</Text>
          </View>
          <Image
            style={{
            width: Dimensions.get("window").height / 2,
            height: Dimensions.get("window").height / 1.5
            }}
            source={{ uri: navigation.state.params.uri }}
          />
          <Button
            text={"Delete this photo"}
            func={()=>delOne(navigation.state.params.id)}
            btStyle={{ flex: 1 }}
            textStyle={{ fontWeight: "bold" }}
          />
        </View>
    );
};
Photo.navigationOptions = {
  headerStyle: {
    backgroundColor: "#f44336"
  },
  headerTitleStyle: {
    color: "#ffcdd2",
    textAlign: "center"
  },
  title: "Zdjęcie"
};

export default Photo;
