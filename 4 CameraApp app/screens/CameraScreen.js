import React, { Component } from "react";
import {
  View,
  Text, StyleSheet, Dimensions
} from "react-native";
import * as Permissions from "expo-permissions";
import { Camera } from "expo-camera";
import * as MediaLibrary from "expo-media-library";
import CircleButton from "../components/CircleButton";
import { Animated } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const wb = [
  "auto",
  "cloudy",
  "fluorescent",
  "incandescent",
  "shadow",
  "sunny",
]
let sizes = []
class CameraScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasCameraPermission: null, // przydzielone uprawnienia do kamery
      type: Camera.Constants.Type.back, // typ kamery
      pos: new Animated.Value(Dimensions.get("window").height-50),  //startowa pozycja y wysuwanego View
      constantsWb: Camera.Constants.WhiteBalance,
      wb: "auto",
      ps: "1600x1200",
      sizes: [],
      whites: [],
    };
    this.snap = this.snap.bind(this);
    this.reverse = this.reverse.bind(this);

    this.isHidden = true
    console.log(this.state.pos)
    // console.log(Camera.Constants.Ratio.getAvailablePictureSizesAsync())
    console.log(Camera.Constants.WhiteBalance)
    
  }
  getSizes = async () => {
    if (this.camera) {
        const sizes = await this.camera.getAvailablePictureSizesAsync("4:3")
        alert(JSON.stringify(sizes, null, 4))
        this.setState({
          sizes: sizes,
        })
    }
  };
  toggle = () => {
      if (this.isHidden) toPos = 30; else toPos = Dimensions.get("window").height-50
      //animacja
      Animated.spring(
        this.state.pos,
        {
          toValue: toPos,
          velocity: 1,
          tension: 0,
          friction: 10,
        }
      ).start();
      this.isHidden = !this.isHidden;
  }
  snap = async () => {
    if (this.camera) {
      let foto = await this.camera.takePictureAsync();
      let asset = await MediaLibrary.createAssetAsync(foto.uri); // domyslnie zapisuje w DCIM
      alert(JSON.stringify(asset, null, 4));
    }
  };
  reverse = () => {
    this.setState({
      type:
        this.state.type === Camera.Constants.Type.back
          ? Camera.Constants.Type.front
          : Camera.Constants.Type.back
    });
  };
  componentDidMount = async () => {
    let { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status == "granted" });
    this.getSizes()
  };
  whiteb = (wb) => {
    this.setState({ wb: wb });
  }
  sizeb = (wb) => {
    this.setState({ ps: wb });
  }

  render() {
    const { hasCameraPermission } = this.state; // podstawienie zmiennej ze state
    if (hasCameraPermission == null) {
      return <View />;
    } else if (hasCameraPermission == false) {
      return <Text>brak dostępu do kamery</Text>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera
            ref={ref => {
              this.camera = ref; // Uwaga: referencja do kamery używana później
            }}
            style={{ flex: 1 }}
            type={this.state.type}
            
            onCameraReady={() => console.log("camera ready")}
            whiteBalance={this.state.wb}
            pictureSize={this.state.ps}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignContent: "center",
                alignItems: "center",
                position: "absolute",
                bottom: 0
              }}
            >
              <CircleButton
                large={false}
                func={this.reverse}
                btStyle={{ flex: 1 }}
                img={"rotate-cw"} 
              />
              <CircleButton
                large={true}
                func={this.snap}
                btStyle={{ flex: 1 }}
                img={"plus"} 
              />
              <CircleButton
                large={false}
                func={this.toggle}
                btStyle={{ flex: 1 }}
                img={"settings"} 
              />
            </View>
            <Animated.View
              style={[
                styles.animatedView,
                {
                  transform: [
                    { translateY: this.state.pos }
                  ]
                }]} >
                <View style={{borderTopColor: "white", borderTopWidth: 2, marginBottom: 40, marginTop: 40}}>
                  <Text style={{color: "white"}}>white balance</Text>
                </View>
                {wb.map((e)=>(
                  <TouchableOpacity onPress={()=>this.whiteb(e)} key={e} style={{width: "100%"}}>
                  <View style={{justifyContent: "flex-start", alignItems: "center", flexDirection: "row", height: 50, width: "100%"}}>
                    <View style={{width: 20, height: 20, borderRadius: 20, backgroundColor: e===this.state.wb?"green":"white"}}>
                      <Text>{" "}</Text>
                    </View>
                    <Text style={{color: e===this.state.wb?"green":"white"}}>{e}</Text>
                    </View>
                  </TouchableOpacity>
                ))}
                <View style={{borderTopColor: "white", borderTopWidth: 2, marginBottom: 40, marginTop: 40}}>
                  <Text style={{color: "white"}}>picture size</Text>
                </View>
                {this.state.sizes.map((e)=>(
                  <TouchableOpacity onPress={()=>this.sizeb(e)} key={e}  style={{width: "100%"}}>
                    <View style={{justifyContent: "flex-start", alignItems: "center", flexDirection: "row", height: 50, width: "100%"}}>
                      <View style={{width: 20, height: 20, borderRadius: 20, backgroundColor: e===this.state.ps?"green":"white"}}>
                        <Text>{" "}</Text>
                      </View>
                      <Text style={{color: e===this.state.ps?"green":"white"}}>{e}</Text>
                    </View>
                  </TouchableOpacity>
                ))}
            </Animated.View>
          </Camera>
        </View>
      );
    }
  }
}

CameraScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#f44336"
  },
  headerTitleStyle: {
    color: "#ffcdd2",
    textAlign: "center"
  },
  title: "Kamera"
};

var styles = StyleSheet.create({
  animatedView: {
      position: "absolute",
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: "#00ff0055",
      height: Dimensions.get("window").height - 50,
      width: Dimensions.get("window").width / 2,
  }
});

export default CameraScreen;
