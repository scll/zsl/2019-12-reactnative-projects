import React from "react"
import { StyleSheet, Text, View, TextInput } from "react-native"
import { EvilIcons } from "@expo/vector-icons";

export default function UserEdit({navigation}) {
    return (
        <View style={styles.main}>
            <View style={styles.container1}>
                <EvilIcons size={362} color={"#f44336"} name="user" />
            </View>
            <View style={styles.container2}>
                <View style={styles.container2}>
                    <Text style={styles.inputHeader}></Text>
                    <Text style={styles.inputHeader}>Nick: {navigation.state.params.nick}</Text>
                    <Text style={styles.inputHeader}>Haslo: {navigation.state.params.pass}</Text>
                    <Text style={styles.inputPlaceholder}></Text>
                </View>
            </View>
        </View>
    )
}
UserEdit.navigationOptions = {
    // header: null,
    title: "Register node app",
    headerStyle: {
      backgroundColor: "#f44336",
    },
    headerTitleStyle: {
      color: "#ffcdd2"
    }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#ffcdd2"
  },
  container1: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    backgroundColor: '#ffcdd2',
  },
  container2: {
    flex: 3,
  },
  inputHeader: {
    flex: 1,
    fontSize: 30,
    height: 60,
    color: '#f44336',
    textAlign: "center",
  },
  inputPlaceholder: {
    flex: 8,
  },
  input: {
    height: 60,
    fontSize: 20,
    borderStyle: 'solid',
    borderBottomColor: '#f44336',
    borderBottomWidth: 2,
  },
})
