import React, { useState, useEffect } from "react";
import { View, FlatList, TouchableOpacity } from "react-native";
import UserItem from "./User";

import S from "../settings";

const Users = ({ navigation }) => {
  const [users, cUsers] = useState("");
  const getUsers = () => {
    return fetch(`http://${S.ip + S.port}/users`)
      .then(response => {
        return response.json();
      })
      .then(responseJSON => {
        cUsers(responseJSON);
      })
      .catch(err => {
        console.log(err);
      });
  };
  navigation.addListener("didFocus", () => {
    getUsers();
  });

  useEffect(() => {
    getUsers();
  }, [1]);

  return (
    <View>
      <FlatList
        data={users}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <TouchableOpacity key={index} style={{ margin: 20, flex: 1 }}>
            <UserItem
              data={item}
              cUsers={cUsers}
              navigation={navigation}
              index={index}
            />
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
Users.navigationOptions = {
    // header: null,
    title: "Register node app",
    headerStyle: {
      backgroundColor: "#f44336",
    },
    headerTitleStyle: {
      color: "#ffcdd2"
    }
}
export default Users;
