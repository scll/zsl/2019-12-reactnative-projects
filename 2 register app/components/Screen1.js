import React from "react"
import { StyleSheet, Text, View, TextInput } from "react-native"
import Button from "./Button"
import S from "../settings";

export default function Screen({navigation}) {
    const [nick, onChangeNick] = React.useState('test');
    const [pass, onChangePass] = React.useState('test');
    const rgstr = (name,pw) => {
        return fetch(`http://${S.ip + S.port}/login`, {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              username: name,
              password: pw
            })
          })
            .then(response => {
              return response.json();
            })
            .then(text => {
              //Handle server confirmation
              console.log(text);
              text.status === "OK"
                ? navigation.navigate("s2")
                : alert("taki uzytkownik juz istnieje");
            })
            .catch(err => {
              console.log(err);
            });
    }
    return (
        <View style={styles.main}>
            <View style={styles.container1}>
                <Text style={styles.container1}>Register Node App</Text>
            </View>
            <View style={styles.container2}>
                <View style={styles.container2}>
                    <TextInput style={styles.input} onChangeText={e => onChangeNick(e)} value={nick} type='text'/>
                    <Text style={styles.inputHeader}>username</Text>
                    <TextInput style={styles.input} onChangeText={e => onChangePass(e) } secureTextEntry={true} value={pass} type='password'/>
                    <Text style={styles.inputHeader}>password</Text>
                </View>
                <Button text={"Register"} action={()=>rgstr(nick,pass)} />
            </View>
        </View>
    )
}

Screen.navigationOptions = {
    header: null,
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#ffcdd2"
  },
  container1: {
    flex: 1,
    fontSize:50,
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#f44336',
  },
  container2: {
    flex: 1,
  },
  inputHeader: {
    height: 60,
    color: '#f44336',
  },
  input: {
    height: 60,
    fontSize: 20,
    borderStyle: 'solid',
    borderBottomColor: '#f44336',
    borderBottomWidth: 2,
  },
})
