import React from "react";
import { View, Text } from "react-native";
import { EvilIcons } from "@expo/vector-icons";
import Button1 from "./Button";

import S from "../settings";

const User = ({ data, cUsers, navigation, index }) => {
    console.log(index)
  const goToEdit = () => {
    navigation.navigate("userEdit", {
      nick: data.username,
      pass: data.password,
    });
  };
  const del = () => {
    return fetch(`http://${S.ip + S.port}/del`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        index: index
      })
    })
      .then(response => {
        return response.json();
      })
      .then(users => {
        //Handle server confirmation
        cUsers(users);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View
      style={{
        flexDirection: "row",
        flex: 1,
        alignContent: "center",
        justifyContent: "space-around",
        alignItems: "center"
      }}
    >
      <EvilIcons size={32} color={"#f44336"} name="user" />
      <Text
        style={{ textAlign: "center", textAlignVertical: "center", flex: 4 }}
      >
        {index}
        {": "}
        {data.username} {data.password}
      </Text>
      <Button1
        action={()=>goToEdit()}
        text={"EDYTUJ"}
        btStyle={{ flex: 1 }}
        textStyle={{ fontWeight: "bold" }}
      />
      <Button1
        text={"USUŃ"}
        action={del}
        btStyle={{ flex: 1 }}
        textStyle={{ fontWeight: "bold" }}
      />
    </View>
  );
};

export default User;
