import React, {useEffect} from "react"
import { StyleSheet, Text, View, Switch, FlatList, Image } from "react-native"
import Button1 from "./Button"
import * as Permissions from "expo-permissions";
import * as Location from "expo-location";
import { AsyncStorage } from "react-native"


export default function Screen({navigation}) {
  const [switchAllValue, toggleAll] = React.useState(false)
  const [savedList, updateSavedList] = React.useState([])
  const setPermissions = async () => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
        alert('odmawiam przydzielenia uprawnień do czytania lokalizacji')
    }
    else {
      const keys = await AsyncStorage.getAllKeys(); 
      const stores = await AsyncStorage.multiGet(keys);
      let newL = stores.map((result, i, store) => {
        return [store[i][0], JSON.parse(store[i][1])]
      });
      updateSavedList(newL)
    }
  }
  const setData = async () => {
    const pos = await Location.getCurrentPositionAsync({})
    const savedPos = [pos.coords.longitude, pos.coords.latitude, false]
    const savedWithKey = [JSON.stringify(pos.timestamp), savedPos]
    await AsyncStorage.setItem(JSON.stringify(pos.timestamp), JSON.stringify(savedPos)); 
    const newList = [...savedList, savedWithKey]
    await updateSavedList(newList)
  }
  const clearData = async () => {
    await AsyncStorage.clear();
    updateSavedList([])
  }
  const getData = () => {
    let tak = false
    for(let i=0;i<savedList.length;i++){
      savedList[i][1][2] === true ? tak = true : null
    }
    tak ? navigation.navigate("mapa", {savedList: savedList}) : alert("Wybierz co najmniej jeden punkt")
  }
  useEffect(() => {
    setPermissions();
  }, []);
  const updateSelected = (timestamp,value) => {
    for(let i=0;i<savedList.length;i++){
      if(savedList[i][0].toString()===timestamp){
        const newList = [...savedList]
        newList[i][1][2] = value
        updateSavedList(newList)
      }
    }
  }
  const mainSwitch = (e) => {
    toggleAll(e)
    for(let i=0;i<savedList.length;i++){
      const newList = [...savedList]
      newList[i][1][2] = e
      updateSavedList(newList)
    }
  }
  return (
    <View style={styles.main}>
      <View style={styles.container1}>
        <Button1 action={()=>setData()} text={"Pobierz i zapisz pozycje"}/>
        <Button1 action={()=>clearData()} text={"Usun wszystkie dane"}/>
      </View>
      <View style={styles.container1}>
        <Text style={styles.switch1} />
        <View style={styles.button1}> 
          <Button1 action={()=>getData()} text={"Przejdz do mapy"} /> 
        </View>
        <View style={styles.switch1}> 
          <Switch 
            onValueChange={(e)=>mainSwitch(e)}
            value={switchAllValue}
          />
        </View>
      </View>
      
      <FlatList
        data={savedList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <View key={index} style={{ flex: 1 }}>
            <View style={styles.main2}>
                <View style={styles.containerImage}>
                  <Image
                    style={{width: 50, height: 50, borderRadius: 25, borderWidth: 2, borderColor: "black"}}
                    source={require('./pico.png')}
                  />
                </View>
                <View style={styles.containerStats}>
                    <Text style={{fontWeight: "bold"}}> timestamp: {item[0]} </Text>
                    <Text> latitude: {item[1][1]} </Text>
                    <Text> longitude: {item[1][0]} </Text>
                </View>
                <View style={styles.containerSwitch}>
                    <Switch
                      onValueChange={()=>updateSelected(item[0],!item[1][2])}
                      value={item[1][2]} 
                    />
                </View>
            </View>
          </View>
        )}
      />
    </View>
  )
}

Screen.navigationOptions = {
  title: "Zapis pozycji",
  headerStyle: {
    backgroundColor: "#f44336",
  },
  headerTitleStyle: {
    color: "#ffcdd2"
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  main2: {
    flex: 1,
    backgroundColor: "#f44336",
    alignContent: "center",
    justifyContent: "space-around",
    alignItems: "center"
  },
  container1: {
    height: 40,
    alignContent: "center",
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row",
  },
  switch1: {
    flex: 1,
  },
  button1: {
    flex: 8,
  },
  main2: {
    flex: 1,
    height: 100,
    marginBottom:10,
    flexDirection: "row",
  },
  containerImage: {
    flex: 1,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "space-around",
  },
  containerStats: {
    flex: 8,
    alignContent: "center",
    alignItems: "flex-start",
    justifyContent: "space-around",
    flexDirection: "column",
    padding: 5,
    paddingLeft: 20,
  },
  containerSwitch: {
    flex: 1,
    alignContent: "center",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around",
  },
})
