import React from "react"
import { View } from "react-native"
import MapView from 'react-native-maps';



export default function Screen({navigation}) {
  const markers = navigation.state.params.savedList.map((e, index)=>{
    const pos = (e[1])
    const title = "pozycja "+ (index+1)
    if(pos[2])
      return (
      <MapView.Marker
          coordinate={{
              latitude: parseFloat(pos[1]),
              longitude: parseFloat(pos[0]),
          }}
          title={title}
          description={"opis"}
          key={e[0]}
      />
  )})
  return (
    <View style={{ flex: 1 }}>
        <MapView
            style={{ flex: 1 }}
            initialRegion={{
                latitude: 49.8518222,
                longitude: 19.8411963,
                latitudeDelta: 0.001,
                longitudeDelta: 0.001,
            }}
        >
          {markers}
        </MapView>
    </View>
  )
}

Screen.navigationOptions = {
  // header: null,
  title: "Mapa",
  headerStyle: {
    backgroundColor: "#f44336",
  },
  headerTitleStyle: {
    color: "#ffcdd2"
  }
}
